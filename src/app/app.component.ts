import { ClearAllTodo } from './shared/stores/todo-list/todo-list.actions';
import { Component } from '@angular/core';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private readonly store: Store) {}

  onClearTodo(): void {
    console.log('helo');

    this.store.dispatch(new ClearAllTodo());
  }
}
