import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { CardTodoInputModule } from './shared/components/card-todo-input/card-todo-input.module';
import { CardTodoModule } from './shared/components/card-todo/card-todo.module';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { TodoListState } from './shared/stores/todo-list/todo-list.store';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxsModule.forRoot([TodoListState]),
    NgxsStoragePluginModule.forRoot({
      key: [TodoListState],
    }),
    //! Component
    CardTodoModule,
    CardTodoInputModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
