import { Action, State, StateContext } from '@ngxs/store';
import {
  AddTodo,
  CheckedTodo,
  ClearAllTodo,
  RemoveTodo,
  UnCheckedTodo,
} from './todo-list.actions';
import { TodoList, TodoListStateModel } from './todo-list-state';

import { Injectable } from '@angular/core';

@State<TodoListStateModel>({
  name: 'todoList',
  defaults: {
    todo_list: [],
  },
})
@Injectable()
export class TodoListState {
  constructor() {}

  @Action(AddTodo)
  addTodo(ctx: StateContext<TodoListStateModel>, action: AddTodo) {
    const state: TodoListStateModel = ctx.getState();
    const currentDate: string = new Date().toISOString();

    state.todo_list.push({
      ...action,
      is_checked: false,
      created_at: currentDate,
    });

    ctx.setState({
      ...state,
      todo_list: state.todo_list,
    });
  }

  @Action(RemoveTodo)
  removeTodo(ctx: StateContext<TodoListStateModel>, action: RemoveTodo) {
    const state: TodoListStateModel = ctx.getState();
    state.todo_list = state.todo_list.filter(
      (todo: TodoList, index: number) => index !== action.index,
    );

    ctx.setState({
      ...state,
      todo_list: state.todo_list,
    });
  }

  @Action(CheckedTodo)
  checkedTodo(ctx: StateContext<TodoListStateModel>, action: CheckedTodo) {
    const state: TodoListStateModel = ctx.getState();
    state.todo_list[action.index].is_checked = true;

    ctx.setState({
      ...state,
      todo_list: state.todo_list,
    });
  }

  @Action(UnCheckedTodo)
  unCheckedTodo(ctx: StateContext<TodoListStateModel>, action: UnCheckedTodo) {
    const state: TodoListStateModel = ctx.getState();
    state.todo_list[action.index].is_checked = false;

    ctx.setState({
      ...state,
      todo_list: state.todo_list,
    });
  }

  @Action(ClearAllTodo)
  clearAllTodo(ctx: StateContext<TodoListStateModel>, action: ClearAllTodo) {
    console.log('work');
    ctx.setState({
      todo_list: [],
    });
  }
}
