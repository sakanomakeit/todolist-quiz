export interface TodoList {
  text: string;
  is_checked: boolean;
  created_at: string;
}

export interface TodoListStateModel {
  todo_list: TodoList[];
}
