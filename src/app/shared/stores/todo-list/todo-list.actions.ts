export class AddTodo {
  static readonly type: string = '[TodoList] AddTodo';
  constructor(public text: string) {}
}

export class RemoveTodo {
  static readonly type: string = '[TodoList] RemoveTodo';
  constructor(public index: number) {}
}

export class CheckedTodo {
  static readonly type: string = '[TodoList] CheckedTodo';
  constructor(public index: number) {}
}

export class UnCheckedTodo {
  static readonly type: string = '[TodoList] UnCheckedTodo';
  constructor(public index: number) {}
}

export class ClearAllTodo {
  static readonly type: string = '[TodoList] ClearAllTodo';
  constructor() {}
}
