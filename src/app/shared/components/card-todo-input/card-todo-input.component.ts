import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AddTodo } from '../../stores/todo-list/todo-list.actions';
import { Store } from '@ngxs/store';

@Component({
  selector: 'card-todo-input',
  templateUrl: './card-todo-input.component.html',
  styleUrls: ['./card-todo-input.component.scss'],
})
export class CardTodoInputComponent implements OnInit {
  todoForm: FormGroup;

  constructor(private store: Store, private readonly _fb: FormBuilder) {}

  ngOnInit() {
    this.onInitForm();
  }

  onInitForm(): void {
    this.todoForm = this._fb.group({
      text: this._fb.control('', [Validators.required]),
    });
  }

  onAddTodo(): void {
    if (!this.todoForm.valid) return;

    const { text } = this.todoForm.value;
    this.store.dispatch(new AddTodo(text));
    this.todoForm.reset();
  }
}
