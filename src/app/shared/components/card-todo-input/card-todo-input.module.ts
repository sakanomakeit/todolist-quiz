import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CardTodoInputComponent } from './card-todo-input.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  declarations: [CardTodoInputComponent],
  exports: [CardTodoInputComponent],
})
export class CardTodoInputModule {}
