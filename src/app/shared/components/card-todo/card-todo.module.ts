import { CardTodoComponent } from './card-todo.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [CommonModule],
  declarations: [CardTodoComponent],
  exports: [CardTodoComponent],
})
export class CardTodoModule {}
