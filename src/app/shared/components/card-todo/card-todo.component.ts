import {
  CheckedTodo,
  RemoveTodo,
  UnCheckedTodo,
} from './../../stores/todo-list/todo-list.actions';
import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';

import { Observable } from 'rxjs';
import { TodoListState } from './../../stores/todo-list/todo-list.store';
import { TodoListStateModel } from './../../stores/todo-list/todo-list-state';

@Component({
  selector: 'card-todo',
  templateUrl: './card-todo.component.html',
  styleUrls: ['./card-todo.component.scss'],
})
export class CardTodoComponent implements OnInit {
  @Select(TodoListState)
  $todoList: Observable<TodoListStateModel>;

  constructor(private readonly store: Store) {}

  ngOnInit() {}

  onChangeChecked(index: number, isChecked: boolean): void {
    if (isChecked) this.store.dispatch(new UnCheckedTodo(index));
    else this.store.dispatch(new CheckedTodo(index));
  }

  onRemoveTodo(index: number): void {
    this.store.dispatch(new RemoveTodo(index));
  }
}
