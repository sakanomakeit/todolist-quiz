// eslint-disable-next-line no-undef
module.exports = {
  mode: 'jit',
  content: ['./src/**/*.{html,ts,scss,css}'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  // eslint-disable-next-line no-undef
  plugins: [require('@tailwindcss/forms')],
};
