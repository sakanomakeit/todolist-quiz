# Todo List Quiz

This project is todo list with Angular ( version 12.2.16 )

## Tools

- Angular (12.2.16)
- Ngxs
- TailwindCss
- Fontawesome (free version)

## Features

- Add todo to list.
- Remove todo from list.
- Check ang uncheck todo.
- Clear all todo list.
- When refreshed, the data persists
- Support Responsive

## How to run project

1). Install tools

```sh
npm install
```

2). Run angular project

```sh
ng serve
```

3). Project run on "localhost:4200" and Enjoin
